Lectures
========

Lecture notes are available :download:`here <db-notes.pdf>` and will be
updated during the course.

You can also access the slides from last year 
`here <http://www.cse.chalmers.se/edu/year/2015/course/TDA357/VT2015/slides/>`_. 
These slides can give a different perspective and thereby help your understanding.

The notes and the slides do not cover everything that is said and given
during the lectures. But they should contain enough of the technical material
so that you can prepare for the exam if you need to skip some lectures. 
To get the full coverage, attend the lectures and read the
:doc:`Course Book <../course_book>`.


Course Plan
~~~~~~~~~~~

The course will contain:

-  Relational model, E-R diagrams
-  Functional dependencies
-  Normalization, BCNF, Multivalued dependencies, 3NF, 4NF
-  SQL DDL, SQL Queries, Relational Algebra
-  Assertions, Triggers
-  SQL/PSM, Embedded SQL, JDBC
-  Transactions, Authorization
-  Indexes
-  XML
-  …

===========   ================
Date          Subject
===========   ================
January 20    :ref:`lecture1`
January 21    :ref:`lecture2`
January 25    :ref:`lecture3`
January 28    :ref:`lecture4`
February 1    :ref:`lecture5`
February 4    :ref:`lecture6`
February 8    :ref:`lecture7`
February 15   :ref:`lecture8`
February 18   :ref:`lecture9`
February 22   :ref:`lecture10`
February 25   :ref:`lecture11`
February 29   :ref:`lecture12`
March 3       :ref:`lecture13`
===========   ================



.. _lecture1:

Lecture 1, Introduction
~~~~~~~~~~~~~~~~~~~~~~~

Book sections
  - |book1e|: 1
  - |book2e|: 1

.. Slides
..   - :download:`one per page <slides1.pdf>`
..   - :download:`six per page <lecture1.pdf>`
..   - :download:`alternative slides <vt2014/lecture1.pdf>`


.. _lecture2:

Lecture 2, The relational data model
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Book sections
  - |book1e|: 3.1, 3.6.1-2, 2, 3.2
  - |book2e|: 2.2, 3.3.1-2, 4.1-6

.. Slides
..   - :download:`one per page <slides2.pdf>`
..   - :download:`six per page <lecture2.pdf>`
..   - :download:`alternative slides <vt2014/lecture2.pdf>`


.. _lecture3:

Lecture 3, E-R diagrams
~~~~~~~~~~~~~~~~~~~~~~~

Book sections
  - |book1e|: 2, 3.2-5
  - |book2e|:  4.1-6, 3.1-2

.. Slides
..   - :download:`one per page <slides3.pdf>`
..   - :download:`six per page <lecture3.pdf>`
..   - :download:`alternative slides <vt2014/lecture3.pdf>`


.. _lecture4:

Lecture 4, Functional dependencies and normal forms
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Book sections
  - |book1e|: 3.6.3-, 3.7
  - |book2e|   3.3.3-4, 3.4.1, 3.5.1, 3.6

.. Slides
..   - :download:`one per page <slides4.pdf>`
..   - :download:`six per page <lecture4.pdf>`
..   - :download:`alternative slides, 1 <vt2014/lecture4.pdf>`
..   - :download:`alternative slides, 2 <vt2014/lecture5.pdf>`


.. _lecture5:

Lecture 5, SQL 1
~~~~~~~~~~~~~~~~

Book sections
  - |book1e|: 6.5-6, 7.1-3, 5.1-2, 6.1.1-2, 6.2.1
  - |book2e|:   6.5, 2.3, 7.1-3, 2.4, 6.1.1-2, 6.2.1

Country data: :download:`countries.tsv`

.. Slides
..   - :download:`one per page <slides5.pdf>`
..   - :download:`six per page <lecture5.pdf>`
..   - :download:`alternative slides <vt2014/lecture6.pdf>`


.. _lecture6:

Lecture 6, SQL2
~~~~~~~~~~~~~~~

Book sections
  - |book1e|: 5.2.9-, 6.1.3 -6, 6.2.2-4, 6.3.1-7, 6.7.1-2
  - |book2e|: 2.4.11-, 6.1.3-7, 6.2.2-4, 6.3.1-7, 8.1

.. Slides
..   - :download:`one per page <slides6.pdf>`
..   - :download:`six per page <lecture6.pdf>`
..   - :download:`alternative slides <vt2014/lecture7.pdf>`


.. _lecture7:

Lecture 7, Table modification, triggers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Book sections
  - |book1e|: rest of 5 & 6 except 6.7
  - |book2e|: 6.1.8, 6.2.5, 6.3.8, 6.4, 5.1, 5.2, 2.5

.. Slides
..   - :download:`one per page <slides7.pdf>`
..   - :download:`six per page <lecture7.pdf>`
..   - :download:`alternative slides <vt2014/lecture8.pdf>`


.. _lecture8:

Lecture 8, Relational algebra, query compilation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Book sections
  - |book1e|: 6.7, 7.1.5, 7.4-
  - |book2e|: 8.1, 8.2, 7.1.2, 7.4-7.5

.. Slides
..   - :download:`one per page <slides8.pdf>`
..   - :download:`six per page <lecture8.pdf>`
..   - :download:`alternative slides <vt2014/lecture9.pdf>`


.. _lecture9:

Lecture 9, Embedded SQL
~~~~~~~~~~~~~~~~~~~~~~~

Book sections
  - |book1e|: 8.1-5
  - |book2e|:  9.2-6

.. Slides
..   - :download:`one per page <slides9.pdf>`
..   - :download:`six per page <lecture9.pdf>`
..   - :download:`alternative slides <vt2014/lecture10.pdf>`
..   - :download:`some extra slides on Embedded SQL and ODBC <lecture9extra.pdf>`


.. _lecture10:

Lecture 10, Transactions, authorization, indexes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Book sections
  - |book1e|: 8.6-7; 6.6.5-6,13.1-2
  - |book2e|:  6.6, 10.1, 18.1; 8.3-4, 14.1

.. Slides
..   - :download:`one per page <slides10.pdf>`
..   - :download:`six per page <lecture10.pdf>`
..   - :download:`alternative slides <vt2014/lecture12.pdf>`
..   - :download:`one per page <slides11.pdf>`
..   - :download:`six per page <lecture11.pdf>`


.. _lecture11:

Lecture 11, XML and NoSQL
~~~~~~~~~~~~~~~~~~~~~~~~~

Book sections
  - |book1e|: 4.6-7
  - |book2e|:  11.1-2

.. Slides
..   - :download:`alternative slides <vt2014/lecture13.pdf>`


.. _lecture12:

Lecture 12, Databases at Spotify (guest lecture by Oscar Söderlund, Spotify)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. _lecture13:

Lecture 13, Exam training
~~~~~~~~~~~~~~~~~~~~~~~~~




.. |book1e| replace:: Database Systems: The Complete Book, first edition
.. |book2e| replace:: Database Systems: The Complete Book, second edition
