Exercises (a.k.a. Tutorials)
============================

.. toctree::
   :hidden:

   3-queries/index
   4-triggers/index
   6-xml/index

This year's exercises will be picked from old exams.

2016-01-27 Exercise 1: Design.
  :download:`Questions <Ex1-questions.pdf>`,
  :download:`Solutions <Ex1-solutions.pdf>`
2016-02-10 Exercise 2: Design.
  :download:`Questions <Ex2-questions.pdf>`,
  :download:`Solutions <Ex2-solutions.pdf>`
2016-02-17 Exercise 3:
  :doc:`3-queries/index`
2016-02-24 Exercise 4:
  :doc:`4-triggers/index`
2016-03-02 Exercise 5: Transactions and JDBC
  :download:`Questions <5-transactions/transaction.pdf>`
  :download:`Slides <5-transactions/Transactionslides.pdf>`
2016-03-09 Exercise 6:
  :doc:`6-xml/index`
