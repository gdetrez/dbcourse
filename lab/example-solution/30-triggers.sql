CREATE OR REPLACE VIEW CourseQueuePosition AS
  SELECT L1.student, L1.course, count(L2.student) AS position
  FROM WaitingList L1 JOIN WaitingList L2 USING (course)
  WHERE L1.place >= L2.place
  GROUP BY L1.student, L1.course
  ORDER BY L1.course, position;

CREATE OR REPLACE FUNCTION register_student() RETURNS TRIGGER AS $$
BEGIN
  -- First, check that the student hasn't passed the course yet
  IF EXISTS (SELECT * FROM PassedCourses
              WHERE student = NEW.student AND course = NEW.course)
    THEN RAISE EXCEPTION 'Course passed already';
  END IF;

  -- Check that the student is not yet registered
  IF EXISTS (SELECT * FROM Registrations
              WHERE student = NEW.student AND course = NEW.course)
    THEN RAISE EXCEPTION 'Already registered';
  END IF;

  -- Check that the student has passed all the prerequisites
  IF EXISTS (SELECT prerequisite FROM Prerequisites WHERE course = NEW.course
              EXCEPT SELECT course FROM PassedCourses WHERE student=NEW.student)
    THEN RAISE EXCEPTION 'Missing prerequisites';
  END IF;

  -- Is the course limited
  IF EXISTS (SELECT * FROM LimitedCourses WHERE course=NEW.course)
    THEN
      -- is the course full?
      IF (SELECT COUNT(*) FROM Registered WHERE course=NEW.course)
          >= (SELECT places FROM LimitedCourses WHERE course=NEW.course)
        THEN INSERT INTO WaitingList VALUES (NEW.student, NEW.course);
        ELSE INSERT INTO Registered  VALUES (NEW.student, NEW.course);
      END IF;
    ELSE
      INSERT INTO Registered  VALUES (NEW.student, NEW.course);
  END IF;
  RETURN NEW;
END
$$ LANGUAGE 'plpgsql';


-- ~~~ UNREGISTER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
DROP TRIGGER IF EXISTS RegisterStudent ON Registrations;
CREATE TRIGGER RegisterStudent INSTEAD OF INSERT on Registrations
FOR EACH ROW EXECUTE PROCEDURE register_student();

CREATE OR REPLACE FUNCTION unregister_student() RETURNS TRIGGER AS $$
DECLARE student_id INTEGER;
BEGIN
  IF OLD.status = 'registered'
    THEN
      DELETE FROM Registered WHERE student=OLD.student AND course=OLD.course;
      -- Is there someone an the waiting list (if so the course must be limited)
      IF EXISTS (SELECT * FROM CourseQueuePosition WHERE course=OLD.course) THEN
          -- is there any seat left?
          IF (SELECT COUNT(*) FROM Registered WHERE course=OLD.course)
               < (SELECT places FROM LimitedCourses WHERE course=OLD.course) THEN
              student_id := (SELECT student FROM CourseQueuePosition
                              WHERE course = OLD.course AND position = 1);
              INSERT INTO Registered VALUES (student_id, OLD.course);
              DELETE FROM WaitingList WHERE student = student_id AND course = OLD.course;
          END IF;
      END IF;
    ELSE
      DELETE FROM WaitingList WHERE student=OLD.student AND course=OLD.course;
  END IF;
  RETURN OLD;
END
$$ LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS UnregisterStudent ON Registrations;

CREATE TRIGGER UnregisterStudent INSTEAD OF DELETE on Registrations
FOR EACH ROW EXECUTE PROCEDURE unregister_student();
