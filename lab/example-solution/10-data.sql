-- Since departments are not really important, let's just create one:
INSERT INTO Departments VALUES ('Science', 'S');

-- Programmes: Having two programmes with the same branch name
INSERT INTO Programmes VALUES
  ('Computer science', 'CS'),
  ('Biology', 'B');
INSERT INTO Branches VALUES
  ('Bioinformatic', 'Computer science'),
  ('Bioinformatic', 'Biology');

-- Students
INSERT INTO Students VALUES
  (1, 'Ponder Stibbons', 'Computer science'),
  (2, 'Adrian Turnipseed', 'Computer science'),
  (3, 'Eskarina Smith', 'Biology'),
  (4, 'Victor Tugelbend', 'Biology'),
  (5, 'Harry Potter', 'Biology');
INSERT INTO StudentBranches VALUES
  (1, 'Computer science', 'Bioinformatic'),
  (3, 'Biology', 'Bioinformatic');

-- ordinary course
INSERT INTO Courses VALUES ('ORD001', 'Ordinary course', 7.5);

-- Full course with folk on the waiting list
INSERT INTO Courses VALUES
  ('FUL001', 'Full course', 7.5);
INSERT INTO LimitedCourses VALUES
  ('FUL001', 1);
INSERT INTO Registered VALUES
  (1, 'FUL001');
INSERT INTO WaitingList VALUES
  (2,'FUL001'),
  (3,'FUL001');

-- Overfull course
INSERT INTO Courses VALUES
  ('FUL002', 'Overfull course', 7.5);
INSERT INTO LimitedCourses VALUES
  ('FUL002', 1);
INSERT INTO Registered VALUES
  (1, 'FUL002'),
  (2, 'FUL002');
INSERT INTO WaitingList VALUES
  (3,'FUL002'),
  (4,'FUL002');

-- Course with prerequisite
INSERT INTO Courses VALUES
  ('ADV001', 'Course with prerequisite', 7.5);
INSERT INTO Prerequisites VALUES
  ('ADV001', 'FUL001');
