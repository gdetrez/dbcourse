-- View: StudentsFollowing
-- For all students, their names, and the programme and branch (if any) they
-- are following.
CREATE OR REPLACE VIEW StudentsFollowing AS
  SELECT id, name, Students.programme, branch
  FROM Students LEFT OUTER JOIN StudentBranches ON id = student;

-- View: FinishedCourses
-- For all students, all finished courses, along with their names, grades
-- (grade 'U', '3', '4' or '5') and number of credits.
CREATE OR REPLACE VIEW FinishedCourses AS
  SELECT id as student, course, grade, credits
  FROM Students
    JOIN HasRead ON id = student
    JOIN Courses ON code = course;

-- View: Registrations
-- All registered and waiting students for all courses, along with their
-- waiting status ('registered' or 'waiting').
CREATE OR REPLACE VIEW Registrations AS
  (SELECT student, course, 'registered' AS status
    FROM Registered)
  UNION
  (SELECT student, course, 'waiting' AS status
    FROM WaitingList);

-- View: PassedCourses
-- For all students, all passed courses, i.e. courses finished with a grade
-- other than 'U', and the number of credits for those courses. This view is
-- intended as a helper view towards the PathToGraduation view, and will not be
-- directly used by your application.
CREATE OR REPLACE VIEW PassedCourses AS
  SELECT student, course, credits
  FROM FinishedCourses
  WHERE grade != 'U';

-- View: UnreadMandatory
-- For all students, the mandatory courses (branch and programme) they have not
-- yet passed. This view is intended as a helper view towards the
-- PathToGraduation view, and will not be directly used by your application.
CREATE OR REPLACE VIEW UnreadMandatory AS
  (SELECT id as student, course
    FROM Students JOIN MandatoryForProgramme USING (programme))
  UNION
  (SELECT student, course
    FROM StudentBranches JOIN MandatoryForBranch USING (branch, programme) )
  EXCEPT
  (SELECT student, course
    FROM PassedCourses);

CREATE VIEW PathToGraduation AS
  WITH
    MandatoryLeft AS
    (SELECT student, COUNT(course) AS mandatory_courses_left
      FROM UnreadMandatory
      GROUP BY student),
    TotalCredits AS
    (SELECT student, SUM(credits) AS total_credits
      FROM PassedCourses
      GROUP BY student),
    BranchCredits AS
    (SELECT student, SUM(credits) AS branch_credits
      FROM StudentBranches
        JOIN PassedCourses USING (student)
        JOIN RecommendedForBranch USING (branch,programme)
      GROUP BY student),
    MathCredits AS
    (SELECT student, SUM(credits) AS math_credits
      FROM PassedCourses JOIN CourseClassifications USING (course)
      WHERE classification = 'math'
      GROUP BY student),
    ResearchCredits AS
    (SELECT student, SUM(credits) AS research_credits
      FROM PassedCourses JOIN CourseClassifications USING (course)
      WHERE classification = 'research'
      GROUP BY student),
    SeminarCount AS
    (SELECT student, COUNT(PassedCourses.course) AS seminar_courses
      FROM PassedCourses JOIN CourseClassifications USING (course)
      WHERE classification = 'seminar'
      GROUP BY student)
  SELECT student, total_credits, mandatory_courses_left, math_credits
          math_credits, research_credits, seminar_courses,
          CASE WHEN branch IS NOT NULL
            AND NOT mandatory_courses_left > 0
            AND branch_credits >= 10
            AND math_credits >= 20
            AND research_credits >= 10
            AND seminar_courses >= 1
            THEN 'yes'
            ELSE 'no'
          END AS can_graduate
  FROM (SELECT id as student FROM Students) Students
    LEFT OUTER JOIN StudentBranches USING (student)
    LEFT OUTER JOIN MandatoryLeft   USING (student)
    LEFT OUTER JOIN BranchCredits   USING (student)
    LEFT OUTER JOIN TotalCredits    USING (student)
    LEFT OUTER JOIN MathCredits     USING (student)
    LEFT OUTER JOIN ResearchCredits USING (student)
    LEFT OUTER JOIN SeminarCount    USING (student);
