CREATE TABLE Departments(
    name            VARCHAR(50)     PRIMARY KEY,
    abbreviation    CHAR(4)         UNIQUE
);

CREATE TABLE Programmes(
    name            VARCHAR(50)     PRIMARY KEY,
    abbreviation    CHAR(4)
);

CREATE TABLE Branches(
    name            VARCHAR(50),
    programme       VARCHAR(50),
    PRIMARY KEY (name, programme),
    FOREIGN KEY (programme)         REFERENCES Programmes
      ON UPDATE CASCADE
      ON DELETE CASCADE
);

CREATE TABLE Students(
    id              INTEGER         PRIMARY KEY,
    name            VARCHAR(50),
    programme       VARCHAR(50),
    UNIQUE          (id,programme),
    FOREIGN KEY     (programme)     REFERENCES Programmes
      ON UPDATE CASCADE
      ON DELETE RESTRICT
);

CREATE TABLE Courses(
    code            CHAR(6)         PRIMARY KEY,
    name            VARCHAR(50),
    credits         FLOAT,
    department      VARCHAR(50),
    FOREIGN KEY     (department)    REFERENCES Departments
      ON UPDATE CASCADE
      ON DELETE RESTRICT
);

CREATE TABLE LimitedCourses(
    course          CHAR(6)         PRIMARY KEY,
    places          INTEGER,

    FOREIGN KEY (course) REFERENCES Courses
      ON UPDATE CASCADE
      ON DELETE CASCADE
);

CREATE TABLE Classifications (
    title           VARCHAR(50)     PRIMARY KEY
);

CREATE TABLE CourseClassifications (
    course          CHAR(6)         REFERENCES Courses
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    classification  VARCHAR(50)     REFERENCES Classifications
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    PRIMARY KEY (course, classification)
);

CREATE TABLE HostedBy(
    department      VARCHAR(50)     REFERENCES Departments
      ON UPDATE CASCADE
      ON DELETE RESTRICT,
    programme       VARCHAR(50)     REFERENCES Programmes
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    PRIMARY KEY (department,programme)
);

CREATE TABLE StudentBranches(
    student INTEGER                 PRIMARY KEY,
    programme       VARCHAR(50),
    branch          VARCHAR(50),
    FOREIGN KEY (student,programme) REFERENCES Students(id,programme)
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    FOREIGN KEY (branch,programme)  REFERENCES Branches(name,programme)
      ON UPDATE CASCADE
      ON DELETE RESTRICT
);

CREATE TABLE Prerequisites(
    course          CHAR(6)         REFERENCES Courses
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    prerequisite    CHAR(6)         REFERENCES Courses
      ON UPDATE CASCADE
      ON DELETE RESTRICT,
    PRIMARY KEY (course,prerequisite)
);

CREATE TABLE MandatoryForProgramme(
    programme       VARCHAR(50)     REFERENCES Programmes
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    course          CHAR(6)         REFERENCES Courses
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    PRIMARY KEY (programme,course)
);

CREATE TABLE MandatoryForBranch(
    programme       VARCHAR(50),
    branch          VARCHAR(50),
    course          CHAR(6)         REFERENCES Courses
      ON UPDATE CASCADE
      ON DELETE RESTRICT,
    PRIMARY KEY (programme,branch,course),
    FOREIGN KEY (branch,programme)  REFERENCES Branches(name,programme)
      ON UPDATE CASCADE
      ON DELETE CASCADE
);

CREATE TABLE RecommendedForBranch(
    programme       VARCHAR(50),
    branch          VARCHAR(50),
    course          CHAR(6),
    PRIMARY KEY (programme,branch,course),
    FOREIGN KEY (branch,programme)  REFERENCES Branches(name,programme)
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    FOREIGN KEY (course)            REFERENCES Courses
      ON UPDATE CASCADE
      ON DELETE RESTRICT
);

CREATE TABLE Registered(
    student         INTEGER         REFERENCES Students
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    course          CHAR(6)         REFERENCES Courses
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    PRIMARY KEY (student,course)
);

CREATE TABLE HasRead(
    student         INTEGER         REFERENCES Students
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    course          CHAR(6)         REFERENCES Courses
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    grade           CHAR(1),
    PRIMARY KEY (student,course),
    CONSTRAINT ValidGrade CHECK (grade IN ('U','3','4','5'))
);

CREATE TABLE WaitingList(
    student         INTEGER         REFERENCES Students
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    course          CHAR(6)         REFERENCES LimitedCourses
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    place           TIMESTAMP       DEFAULT clock_timestamp(),
    PRIMARY KEY (student,course),
    UNIQUE (course,place)
);
